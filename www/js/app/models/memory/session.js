define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        Backbone            = require('backbone'),


		session = Backbone.Model.extend({
		
			url: 'http://meerkat.buzz4healthtest.c66.me/api/v1/sessions.json'
		});
		

    return  session;
        
    

});