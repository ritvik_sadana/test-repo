define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        Backbone            = require('backbone'),

        Repo = Backbone.Model.extend({
        	urlRoot : "http://meerkat.buzz4healthtest.c66.me/medical_cases"
        }),

        RepoCollection = Backbone.Collection.extend({
            url : "http://meerkat.buzz4healthtest.c66.me/api/v1/medical_cases.json",
            model: Repo
        })

    return {
        Repo : Repo,
        RepoCollection : RepoCollection
    };

});