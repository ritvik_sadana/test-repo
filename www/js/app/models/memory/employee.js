define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        Backbone            = require('backbone'),


		Employee = Backbone.Model.extend({
		
			urlRoot: 'http://meerkat.buzz4healthtest.c66.me/api/v1/users'
		}),
		

        EmployeeCollection = Backbone.Collection.extend({
            url : "http://meerkat.buzz4healthtest.c66.me/users.json",
            model: Employee
        })


    return{  
		Employee: Employee,
        EmployeeCollection: EmployeeCollection
    }

});