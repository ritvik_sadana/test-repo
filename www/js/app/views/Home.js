define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        EmployeeListView    = require('app/views/EmployeeList'),
        models              = require('app/models/memory/employee'),
        tpl                 = require('text!tpl/Home.html'),

        template = _.template(tpl);


    return Backbone.View.extend({

        initialize: function () {
            this.employeeList = new models.EmployeeCollection();
            this.render();
        },

        render: function () {
            this.$el.html(template());
            this.listView = new EmployeeListView({collection: this.employeeList, el: $(".scroller", this.el)});
            return this;
        },

        events: {
            "keyup .search-key":    "search",
            "keypress .search-key": "onkeypress"
        },

        search: function (event) {
            var searchField = $('.search-key').val();
            var search_params = {
              'q': searchField,
              'search_by': 'name',
            };
            this.employeeList.fetch({reset: true, data: $.param(search_params)});
        },

        onkeypress: function (event) {
            if (event.keyCode === 13) { // enter key pressed
                event.preventDefault();
            }
        }

    });

});