define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        tpl                 = require('text!tpl/login.html'),
        Session             = require('app/models/session'),
        template = _.template(tpl);
       
    return Backbone.View.extend({

        initialize: function () {
             this.render();
        },

        render: function () {
            this.$el.html(template());
        },

        events: {
            "click #login": "login"
        },

         login: function () {
            console.log('entered');
            var sessionVar = new Session();

            sessionVar.set({
              user: {email: this.$el.find("#email").val(), password: this.$el.find("#password").val()}
            });

            sessionVar.save(sessionVar.attributes,
                    {
                    success: function (userSession, response) {
                        var auth_token = response.data.auth_token,
                            login      = response.data.login,
                            userid     = response.data.id;


                        localStorage.setItem("userid", userid);
                        localStorage.setItem("auth_token", auth_token); 
                        localStorage.setItem("login", login); 

                       // alert(localStorage.getItem("userid"));
                       // alert(localStorage.getItem("login"));
                       // alert(localStorage.getItem("auth_token"));

                    },

                    error: function (userSession, response) {
                        alert('fail');
                        // handle the error code here
                    }
                });
        }


    });

});