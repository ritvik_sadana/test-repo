define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        authmodel           = require('app/models/memory/authmodel'),
        tpl                 = require('text!tpl/Register.html'),

        template = _.template(tpl);

    return Backbone.View.extend({
       

        initialize: function () {
            this.render();
        },
        render: function () {

            this.$el.html(template());
        },

        events: {
            "click #submit" : "register"
        },

        register: function (event) {
            var a = $("#fullname").val();
			var b = $("#email").val();
			var c = $("#profession").val();
			var d = $("#day").val();
			var e = $("#speci").val();

        console.log(a);
        console.log(b);
        console.log(c);
        console.log(d);
        console.log(e);

        var Auth = new authmodel();

            Auth.set({
                user:{ email: b, name: a, persona: c, preferred_tags: d, specilizations: e}
            });
            
            Auth.save(Auth.attributes,
            {
                success: function (data) {
                    alert("done");
                    // body...
                },
                error: function(){
                    alert("you did something wrong");
                }
            })
        }

    });


});