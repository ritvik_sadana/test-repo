define(function (require) {

    "use strict";

    var $                   = require('jquery'),
        _                   = require('underscore'),
        Backbone            = require('backbone'),
        CaseListView	    = require('app/views/CaseList'),
        models              = require('app/models/memory/case'),
        tpl                 = require('text!tpl/CaseHome.html'),

        template = _.template(tpl);


    return Backbone.View.extend({

        initialize: function () {
            this.caseList = new models.RepoCollection();
            this.render();
        },

        render: function () {
            this.$el.html(template());
            this.listView = new CaseListView({collection: this.caseList, el: $(".scroller", this.el)});
            var search_params = {
              'auth_token': localStorage.getItem("auth_token")
            };
            this.caseList.fetch({reset: true, data: $.param(search_params)});
            return this;
        },

        events: {
            "click #submit_value" : "search"
        },

        search: function (event) {

			var searches = $('.search-key').val();
			var search_params = {
			  'auth_token': localStorage.getItem("auth_token"),
			  'specializations[]' : searches
			};
            this.caseList.fetch({reset: true, data: $.param(search_params)});
        }

    });

});
