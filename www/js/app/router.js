define(function (require) {

    "use strict";

    var $           = require('jquery'),
        Backbone    = require('backbone'),
        PageSlider  = require('app/utils/pageslider'),
        HomeView    = require('app/views/Home'),
        CaseView    = require('app/views/CaseHome'),
        usrlogin    = require('app/views/Userlogin'),
        RegistrationView=require('app/views/Register'),

        slider = new PageSlider($('body')),

        homeView = new HomeView(),
        usrLoginView    = new   usrlogin(),
        caseView = new CaseView(),
        registrationView = new RegistrationView();

    return Backbone.Router.extend({

        routes: {
            "": "home",
            "employees/:id": "employeeDetails",
            "medical_List" : "fetchMedicalList",
            "medical_Lists/:id" : "CaseDetails",
            "register": "userRegisteration",
            "login": "performlogin"
        },

        home: function () {
            homeView.delegateEvents();
            slider.slidePage(homeView.$el);
        },
        employeeDetails: function (id) {
            require(["app/models/memory/employee", "app/views/Employee"], function (models, EmployeeView) {
                var employee = new models.Employee({id: id + '.json'});
                employee.fetch({
                    success: function (data) {
                        slider.slidePage(new EmployeeView({model: data}).$el);
                    }
                });
            });
        },

        fetchMedicalList : function() {
            caseView.delegateEvents();
            slider.slidePage(caseView.$el);
        },

        CaseDetails: function (id) {
            require(["app/models/memory/case", "app/views/Case"], function (models, CaseView) {
                var employee = new models.Repo({id: id + '.json'});
                var search_params = {
                    'auth_token': localStorage.getItem("auth_token")
                };
                employee.fetch({data: $.param(search_params),
                    success: function (data) {
                        slider.slidePage(new CaseView({model: data}).$el);
                    }
                });
            });
        },

        performlogin: function() {
            usrLoginView.delegateEvents();
            slider.slidePage(usrLoginView.$el);

        },


        userRegisteration: function(){

                registrationView.delegateEvents();     

                slider.slidePage(registrationView.$el);
            
        }

    });

});